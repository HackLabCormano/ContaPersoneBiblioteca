ContaPersone
============

Realizzazione "homemade" di un sensore per contare gli ingressi/uscite dall'edificio.

Due fotocellule (ex ascensori) di recupero lette da un Wemos/ESP8266 per capire in che direzione si sta muovendo chi passa davanti al sensore, i dati vengono salvati in loco e inviati via MQTT.
