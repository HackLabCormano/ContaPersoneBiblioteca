/**
    ContaPersone per biblioteca Cormano

	Copyright (C) 2018 HackLabCormano (http://hacklabcormano.it)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*
*/

// BOARD: wemos D1 R1 (max filesystem)

// https://github.com/nailbuster/esp8266FTPServer !!!
// ncftp3 -u contapersone -p contapersone  esp8266-21626c.local

// per verificare arrivo messaggi MQTT
// mosquitto_sub -h mqtt.hacklabcormano.it -v -t \#

// TODO uniformare a inglese

// esp8266-21626c.local (BIBLIO) address per distinguere OTA da barra contaprestiti
// esp8266-3d5f86.local (CASA) address per distinguere OTA da barra contaprestiti

/**
 * due "stream" dati:
 * - mqtt (ma non vincolante), pubblica varie notizie, in ordine sparso e con tempistiche varie
 * 		timestamp (solo quando sync ntp, ogni ora)
 * 		heap (debugging, "sono vivo", ogni minuto circa)
 * 		i/u (all'evento)
 * 		presenze (all'evento o in altro caso, RETAINED)
 * - logfile (così è anche selfcontained e non abbiamo dipendenze da servizi esterni)
 * 		timestamp (ogni tanto)
 * 		i/u
 * 		i/u
 * 		i/u
 * 		...
 *
 * legge impulsi sui due pin (fotocellule) per
 * capire se c'è stato un ingresso e in che direzione
 *
 * casi:
 * 1) h1,h2
 * 2) h2,h1
 * 3) h1,...
 * 4) h2,...
 * più le tempistiche!
 *
 * filesystem save dati
   cfr. https://github.com/esp8266/Arduino/blob/master/doc/filesystem.rst

   (old) immaginando di usare 13 car a evento e circa 1000 eventi al giorno ci stanno circa 220 gg in 3M

   (miglioria) timestamp ogni ora con presenze, poi solo I/O, quindi:
   ~20 car * 24h/gg + 1 car a evento

   cioè circa 1500 byte/gg = 2000 giorni!!!
                             ^^^^^^^^^^^^^^
*/

///////////////////////////////////////////////////////////////////////
#include <TaskScheduler.h>
#include <ESP8266WiFi.h>
//#include <ESP8266FtpServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
//#include <ESP8266HTTPUpdateServer.h>
#include <ArduinoOTA.h>
#include <NTPClient.h>
//#include <WiFiUdp.h>
//#include <WiFiClient.h>
//#include <SPI.h>
//#include <SD.h>
#include <PubSubClient.h>  // la lib è PubSubClient (https://pubsubclient.knolleary.net/)
#include <FS.h>

//#define TESTING // per testing (aumenta output)

#define VERSION "web server"

//#define PERIODICSHOW
//#define NODEMCU

//#define DEBUG_INT
//#define KEEPALIVE
#define MQTT_EVENT

#ifdef TESTING
#define TOPIC "ContaPersoneTesting"
#else
#define TOPIC "ContaPersone"
#endif

#define DATACERTA 1537545037 // certamente passata
#define WIFI_TENTATIVI 10

#define LOG // se definita scrive log
#define LOGFILE "/log" // ricordarsi la "/" all'inizio'!!!
#define INGRESSIFILE "/ingressi"
#define USCITEFILE "/uscite"
#define MAXLOGSIZE 200000
boolean spiffsok=false;
/* files (ora spostato, apro e chiudo ogni volta come all'inizio, vediamo se funziona)
File fileLog;
File filePresenze;
*/

#define BLINK_DELAY 10
#define LED D4 // LoLin (non so wemos, cmq è nascosto nella scatola)

// pins (attenzione a sceglierli! non bisogna sovrapporsi ad esempio a LED_BUILTIN)
#define S1	D5 //D1
#define S2	D6 //D2

const int mute=300; // tempo di muting fra una lettura e l'altra (bounces, gambe)
const int decay=2000; // oltre questo tempo azzera h1 e h2 (elimina le "mezze letture")

// timestamps (millis da accensione)
volatile long h1=0; // high1
volatile long h2=0; // high2
long lastFix=0; // timestamp ultima lettura valida (completa)
long lastDiff=0; // distanza tra impulsi (ultima misurata, valida solo se recente)
long last=0; // timestamp ultima lettura (s1 o s2)

// contatori
//int presenze=0; // tolgo perché ridondante rispetto a salvare ingressi e uscite
int ingressi=0; //dall'ultimo reset
int uscite=0; //dall'ultimo reset

const String ssid = "hacklabcormano.it";
const String password = "arduino2linux";

boolean sent=false; // flag per invio MQTT
const String mqtt_server = "mqtt.hacklabcormano.it";

WiFiClient wifiClient;
PubSubClient mqtt_client(wifiClient);
#define DELAY_MQTT 1000
//long lastMsg = 0;
#define MSG_LEN 150
char msg[MSG_LEN];

IPAddress gateway;
byte mac[6];

WiFiUDP ntpUDP; // ntp ci serve per i timestamp!!!
//NTPClient timeClient(ntpUDP,+60*60); // UTC+1
NTPClient timeClient(ntpUDP); // UTC

#define WEBPORT 8080

ESP8266WebServer httpServer(WEBPORT);
//ESP8266HTTPUpdateServer httpUpdater;
//FtpServer ftpSrv;   //set #define FTP_DEBUG in ESP8266FtpServer.h to see ftp verbose on serial

Scheduler runner;

String status() {
    String s = String(timeClient.getEpochTime());
    s+=", I:";
    s+=String(ingressi);
    s+=", U:";
    s+=String(uscite);
    return s;
}

String system() {
    String s = String(timeClient.getEpochTime());
    s+=", heap:";
    s+=String(ESP.getFreeHeap());
    s+=", spiffsok:";
    s+=String(spiffsok);
    s+=", I:";
    s+=String(ingressi);
    s+=", U:";
    s+=String(uscite);
    s+=", logsize:";
    s+=String(spiffs_logSize());
    /*
    s+=", lastFix:";
    s+=String(lastFix);
    s+=", lastDiff:";
    s+=String(lastDiff);
    s+=", version:";
    s+=String(VERSION);
    */
    return s;
}

void sendStatus() {
    mqtt_send(TOPIC "/status",status());
}

void sendSystem() {
    mqtt_send(TOPIC "/system",system());
}

void mqtt_reconnect() {
    noInterrupts();
    if (!mqtt_client.connected()) {
        Serial.print("Attempting MQTT connection...");
        if (mqtt_client.connect("ContaPersone")) {  // si connette al BROKER dichiarando "identità"
            Serial.println("connected");
        } else {
            Serial.print("failed, rc=");
            Serial.println(mqtt_client.state());
        }
    }
    interrupts();
}

void mqtt_send(String topic,String payload) {
    noInterrupts();
    if (WiFi.status() == WL_CONNECTED) {
        if (!mqtt_client.connected()) {
            mqtt_reconnect();
            Serial.println("MQTT reconnected...");
        }

        if (mqtt_client.connected()) {
            //mqtt_client.loop();
            if (!mqtt_client.publish(topic.c_str(), payload.c_str())) {
                Serial.println("MQTT error (connection error or message too large)");
            }
        }
    } else
        Serial.println("no wifi!!!");
    interrupts();
}

void setup_mqtt() {
    mqtt_client.setServer(mqtt_server.c_str(), 1883); // 1883 è porta default MQTT
    //mqtt_client.setCallback(mqtt_callback);
    mqtt_reconnect();
    Serial.println("MQTT set!");
}

/** se passato troppo tempo (da una qualunque delle letture) resetta tutte le letture */
boolean checkVecchio() {
    long t=millis();
    if(
        (h1!=0 && t-h1 >= decay)
        ||
        (h2!=0 && t-h2 >= decay)
    ) {
        h1=0;
        h2=0;
        return true;
    }
    return false;
}

int direzione(long diff) {
    // direzione verificata sul campo
    if(diff>0) return -1;
    if(diff<0) return 1;
    return 0;
}

String direzioneStr(long diff) {
    if(direzione(diff)==1) return String("I");
    if(direzione(diff)==-1) return String("O");
    return "NON DOVREBBE SUCCEDERE!!! (diff==0, salvo al boot/reset)";
}

void spiffs_sanity() {
    File f = SPIFFS.open(LOGFILE, "r");
    if (!f) {
        Serial.println("(checkfs) log open failed, formatting...");
        mqtt_send(TOPIC "/spiffs","formatting");
        spiffsok=SPIFFS.format();
    }
}

void spiffs_log(String v) {
    File f = SPIFFS.open(LOGFILE, "a");

    if(f.size()>MAXLOGSIZE) {
        f.flush();
        f.close();
        f = SPIFFS.open(LOGFILE, "w");
    }

    if (!f) {
        Serial.println("file open (in append) failed");
        spiffsok=false;
        return;
    }
    spiffsok=true;
    f.print(v);
    f.flush();
    f.close();
}

int spiffs_logSize() {
    int s=-1; // errore di fs o file mancante
    File f = SPIFFS.open(LOGFILE, "r");
    if (!f) {
        Serial.println("file open failed");
        spiffsok=false;
        return s;
    }
    spiffsok=true;
    s=f.size();
    f.close();
    return s;
}

void spiffs_getPresenze() {
    File f = SPIFFS.open(INGRESSIFILE, "r");
    if (!f) {
        Serial.println("file open failed");
        spiffsok=false;
        return;
    }
    spiffsok=true;
    ingressi=f.readStringUntil('\n').toInt();
    f.close();

    f = SPIFFS.open(USCITEFILE, "r");
    if (!f) {
        Serial.println("file open failed");
        spiffsok=false;
        return;
    }
    spiffsok=true;
    uscite=f.readStringUntil('\n').toInt();
    f.close();
}

void spiffs_setPresenze() {
    File f = SPIFFS.open(INGRESSIFILE, "w");
    if (!f) {
        Serial.println("file open failed");
        spiffsok=false;
        return;
    }
    spiffsok=true;
    f.println(ingressi);
    f.flush();
    f.close();

    f = SPIFFS.open(USCITEFILE, "w");
    if (!f) {
        Serial.println("file open failed");
        spiffsok=false;
        return;
    }
    spiffsok=true;
    f.println(uscite);
    f.flush();
    f.close();
}

/** metodo principale, viene invocato per verificare e registrare un ingresso/uscita valido */
void fix() {
    if(h1==0 || h2==0 || checkVecchio()) return; // devono esserci entrambe le misure e non essere troppo vecchie

    long now=millis();
    if(now-lastFix <= mute) return; // deve passare almeno "mute" tempo prima di riconsiderare un altro fix

    // da qui in poi la misura è valida
    lastFix=now;
    lastDiff=h1-h2; // il segno dice se entrata/uscita

#ifdef DEBUG_INT
    Serial.print("(fix) h1: ");
    Serial.print(h1);
    Serial.print(", h2: ");
    Serial.println(h2);
#endif

    h1=0;
    h2=0;
    int dir=direzione(lastDiff);
    //presenze+=dir;

    if(dir==1) ingressi++;
    if(dir==-1) uscite++;

    spiffs_setPresenze(); // salva stato delle variabili

#ifdef DEBUG_INT
    Serial.print("fix: ");
    Serial.print(dir);
    Serial.print(", lastDiff: ");
    Serial.print(lastDiff);
    Serial.print(", ingressi: ");
    Serial.println(ingressi);
    Serial.print(", uscite: ");
    Serial.println(uscite);
#endif

#ifdef LOG
    // logga ingresso/uscita
    spiffs_log(direzioneStr(lastDiff));
#endif

#ifdef MQTT_EVENT
    // invia ogni singolo evento
    mqtt_send(TOPIC "/in-out", direzioneStr(lastDiff));
#endif

    sent=false; // ogni fix resetta situazione per cui va inviata al prossimo giro
}

void intS1() {
    noInterrupts();
    //digitalWrite(LED, LOW); // accende
    h1=millis();
    //digitalWrite(LED, HIGH); // spegne
    interrupts(); // IMPORTANTE RIATTIVARLI altrimenti non funzia più nulla!!!
}

void intS2() {
    noInterrupts();
    //digitalWrite(LED, LOW); // accende
    h2=millis();
    //digitalWrite(LED, HIGH); // spegne
    interrupts(); // IMPORTANTE RIATTIVARLI altrimenti non funzia più nulla!!!
}

boolean setup_spiffs() {
#ifdef ESP32       //esp32 we send true to format spiffs if cannot mount
    SPIFFS.begin(true);
#elif defined ESP8266
    SPIFFS.begin();
#endif
    spiffs_sanity();
    spiffs_getPresenze();
    Serial.print("(spiffs init) I/O: ");
    Serial.print(ingressi);
    Serial.print(",");
    Serial.println(uscite);
    //Serial.println("SPIFFS opened!");
}

void setup_sensori() {
#ifdef NODEMCU
    pinMode(S1,INPUT_PULLUP);
    pinMode(S2,INPUT_PULLUP);
#else
    pinMode(S1,INPUT);
    pinMode(S2,INPUT);
#endif

    pinMode(LED, OUTPUT);

#ifdef NODEMCU
    attachInterrupt(digitalPinToInterrupt(S1),intS1, FALLING);
    attachInterrupt(digitalPinToInterrupt(S2),intS2, FALLING);
#else
    attachInterrupt(digitalPinToInterrupt(S1),intS1, RISING);
    attachInterrupt(digitalPinToInterrupt(S2),intS2, RISING);
#endif

    Serial.println("OK sensori!");
}

boolean setup_wifi() {
    delay(500);

    WiFi.mode(WIFI_STA);
    WiFi.hostname(TOPIC);

    // We start by connecting to a WiFi network
    Serial.print("=== Connecting to: ");
    Serial.println(ssid);

    WiFi.disconnect();
    WiFi.begin(ssid.c_str(), password.c_str());

    for (int i=0; (WiFi.status() != WL_CONNECTED) && (i < WIFI_TENTATIVI) && digitalRead(0)==HIGH; i++) {
        delay(500);
        Serial.print(".");
    }

    if(WiFi.status() == WL_CONNECTED) {
        Serial.println();
        Serial.println("+++ WiFi connected!");

        Serial.print("IP: ");
        Serial.println(WiFi.localIP());

        Serial.print("GW: ");
        Serial.println(WiFi.gatewayIP());

        timeClient.begin();
        delay(500);
        timeClient.update();

        // Port defaults to 8266
        ArduinoOTA.setPort(8266);

        // Hostname defaults to esp8266-[ChipID]
        ArduinoOTA.setHostname(TOPIC);

        // No authentication by default
        ArduinoOTA.setPassword((const char *)"conta");

        ArduinoOTA.onStart([]() {
            Serial.println("OTA Start");
        });
        ArduinoOTA.onEnd([]() {
            Serial.println("OTA End");
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("OTA Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
            else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
            else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
            else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
            else if (error == OTA_END_ERROR) Serial.println("End Failed");
        });

        ArduinoOTA.begin();
        Serial.println("+++ OTA Ready");


        httpServer.on("/log", HTTP_GET, http_log);

        //httpUpdater.setup(&httpServer);

        //MDNS.begin(TOPIC);
        //MDNS.addService("http", "tcp", WEBPORT);
        //Serial.println("+++ webserver&WebOTA Ready");

        //httpServer.onNotFound(http_handleNotFound);
        httpServer.begin();
        Serial.println("+++ webserver Ready");

        return true;
    } else {
        Serial.println("--- WiFi FAILED!");
        return false;
    }
}

/** sync ntp, controlla anche wifi,
 * manda mqtt status
 * e logga timestamp su file
 * e appende timestamp in log
*/
#ifdef TESTING
Task taskSyncNTPetc(5*TASK_MINUTE, TASK_FOREVER,
#else
Task taskSyncNTPetc(30*TASK_MINUTE, TASK_FOREVER,
#endif
[]() {
    // TODO disabilitare interrupt anche qui???

    if(timeClient.getHours() == 1 && timeClient.getMinutes() < 30) {
        Serial.println("reset time...");
		mqtt_send(TOPIC "/resetcounters",status());
        ingressi=0;
        uscite=0;
        spiffs_setPresenze();
        ESP.reset();
    }

    if(WiFi.status() != WL_CONNECTED) {
        Serial.println("no wifi! RESETTING!");
        ESP.reset();
    }

    for(int tentativi=0;
            timeClient.getEpochTime() < DATACERTA
            &&
            tentativi < 3;
            tentativi++)
        timeClient.update();    // sync NTP (ZULU/UTC), quasi inutile andare avanti se non c'è time sync

    if(timeClient.getEpochTime() < DATACERTA) {
        Serial.println("no ntp!");
        ESP.reset();
    }

    Serial.print("(NTP sync) Zulu Time: ");
    Serial.println(timeClient.getFormattedTime());
    Serial.print("(NTP sync) EpochTime: ");
    String epoch=String(timeClient.getEpochTime());
    Serial.println(epoch);

    sendSystem();

#ifdef LOG
    String l="\nT:"+String(epoch)+",I:"+String(ingressi)+",U:"+String(uscite)+"\n";
    spiffs_log(l);
#endif
});

/*
Task taskReset(30*TASK_MINUTE, TASK_FOREVER,
[]() {
        Serial.println("reset time...");
        ingressi=0;
        uscite=0;
        spiffs_setPresenze();
        ESP.reset();
});
*/

/** pubblica conteggio */
#ifdef TESTING
// veloce solo per debug, poi rallentare
Task taskPubblicaPresenze(10*TASK_SECOND, TASK_FOREVER,
#else
Task taskPubblicaPresenze(TASK_MINUTE, TASK_FOREVER,
#endif
[]() {
#ifdef TESTING
    sent=false;
#endif
    if(!sent) { // mando solo se è stato aggiornato (cfr. fix())
        sendStatus();
        sent=true;
    }
    sendSystem(); // mando sempre, serve anche per vedere se è vivo
});

#ifdef PERIODICSHOW
/** prove varie di debug */
Task taskShow(5*TASK_SECOND, TASK_FOREVER, []() {
    Serial.println("(periodic)");
    Serial.println(status());
    Serial.println(system());
});
#endif

///////////////////////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println("booting...");

    // vari setup
    setup_wifi();
    setup_mqtt();
    setup_spiffs();
    setup_sensori();

    //ftpSrv.begin("contapersone","contapersone");    //username, password for ftp.  set ports in ESP8266FtpServer.h  (default 21, 50009 for PASV)
    //Serial.println("FTP started");

    // Tasks
    runner.init();

#ifdef PERIODICSHOW
    runner.addTask(taskShow);
    taskShow.enable();
#endif

    runner.addTask(taskPubblicaPresenze);
    taskPubblicaPresenze.enable();

    runner.addTask(taskSyncNTPetc);
    taskSyncNTPetc.enable();

    Serial.println("booted!");
}

////////////////////////////////////////////////////////////////////////////////////
void loop() {
    // IMPORTANTE, non togliere le righe qui sotto!!!
    fix(); // tenta un fix di lettura (non è detto vada a buon fine, dipende dallo stato delle letture)
    runner.execute(); // TaskScheduler
    ArduinoOTA.handle(); // OTA update
    // FIN QUI tenere

    // queste invece si possono mettere e togliere per fare prove di servizi
    httpServer.handleClient(); // Web server (vedi anche in wifi setup per associazione metodi), serve anche per WebOTA
    //ftpSrv.handleFTP();    // FTP server
}

// TODO completamente, per ora solo incollato da esempio SDWebServer
bool http_log() {
    String dataType = "text/plain";

    File dataFile = SPIFFS.open(LOGFILE,"r");
    if(!dataFile) return false;

    //if (server.hasArg("download")) dataType = "application/octet-stream";

    if (httpServer.streamFile(dataFile, dataType) != dataFile.size()) {
        Serial.println("Sent less data than expected!");
        return false;
    }

    dataFile.close();
    return true;
}
