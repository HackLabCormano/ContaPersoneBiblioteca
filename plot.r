#!/usr/bin/env Rscript
suppressMessages(library(lubridate))
suppressMessages(library(anytime))

resol=100

dati = read.csv("log.csv")
dati$Data <- anytime(dati$Data)
dati=dati[dati$Data <= "2019-03-15" | dati$Data >= "2019-03-31",] # elimina periodo sporco

dati$Ora <- hour(dati$Data)
minOra=6
dati=dati[dati$Ora >= minOra ,] # elimina orari inutili

dati$Gsettimana <- wday(dati$Data)
dati$Settimana <- week(dati$Data)
dati$Mese <- month(dati$Data)

dati$NuoviIngressi <- c(0,diff(dati$IngressiCumulo))
dati=dati[dati$NuoviIngressi >= 0 ,] # elimina i negativi (reset notturno)

dati$NuoveUscite <- c(0,diff(dati$UsciteCumulo))
dati=dati[dati$NuoveUscite >= 0 ,] # elimina i negativi (reset notturno)

dati$IngressiMenoUscite <- dati$IngressiCumulo-dati$UsciteCumulo
dati$IngressiMenoUsciteAbs <- abs(dati$IngressiMenoUscite)
#dati$IngressiSumUsciteDiv2 <- (dati$Ingressi+dati$Uscite)/2
#dati$Passaggi <- c(dati$PassaggiCumulativi[1],diff(dati$PassaggiCumulativi))
#dati$PresenzeStimate <- c(dati$IngressiSumUsciteDiv2[1],diff(dati$IngressiSumUsciteDiv2))
#dati$PresenzeStimate <- c(dati$IngressiMenoUsciteAbs[1],diff(dati$IngressiMenoUsciteAbs))
#dati=dati[dati$PresenzeStimate >= 0 ,] # elimina presenze negative

dati$RapportoInOut <- dati$NuoviIngressi/dati$NuoveUscite

#write.csv(dati, file = "log-augmented.csv",row.names=FALSE)
write.csv(dati, file = "log-augmented.csv")

###################################################################
clock.plot <- function (x,nm, color, ...) {
  if( min(x)<0 ) x <- x - min(x)
  if( max(x)>1 ) x <- x/max(x)
  n <- length(x)
  #if(is.null(names(x))) names(x) <- 0:(n-1)
  m <- 1.05
  plot(0, 
	   type = 'n', # do not plot anything
	   xlim = c(-m,m), ylim = c(-m,m), 
	   axes = F, xlab = '', ylab = '', ...)
  a <- pi/2 - 2*pi/200*0:200
  polygon( cos(a), sin(a) )
  v <- .02
  a <- pi/2 - 2*pi/n*0:n
  segments( (1+v)*cos(a), (1+v)*sin(a), 
			(1-v)*cos(a), (1-v)*sin(a) )
  segments( cos(a), sin(a), 
			0, 0, 
			col = 'light grey', lty = 3) 
  ca <- -2*pi/n*(0:50)/50
  for (i in 1:n) {
	a <- pi/2 - 2*pi/n*(i-1)
	b <- pi/2 - 2*pi/n*i
	polygon( c(0, x[i]*cos(a+ca), 0),
			 c(0, x[i]*sin(a+ca), 0),
			 col=color )
	v <- .1
	text((1+v)*cos(a), (1+v)*sin(a), nm[i])
  }
}

genPlot <- function(dati,etichetta){
	summ=summary(dati)
	print(summ)

	#1
	jpeg(paste(etichetta,"ingressi.jpg",sep="-"),width=30,height=15,units="cm",res=resol)
	par(las=3) # xaxis label verticale
	plot(dati$NuoviIngressi~dati$Data,type="h",xlab="",ylab="ingressi")
	regressione <- lm(dati$NuoviIngressi ~ dati$Data)
	abline(regressione, col = "blue", lwd = 4)
	legend("topright", paste("coefficiente trend line:", regressione$coefficients[2]), text.col = "blue")
	title(paste("Ingressi (approx.) Biblioteca Cormano (",etichetta,")",sep=""))
	axis.POSIXct(1,at = seq(min(dati$Data), max(dati$Data), "days"),format="%a %d/%m")
	dev.off()

	#2
	jpeg(paste(etichetta,"heatOraria-clockplot.jpg",sep="-"),width=15,height=15,units="cm",res=resol)
	byhour	<- aggregate(dati$NuoviIngressi, by = list(ora=dati$Ora), mean)
	colnames(byhour)[colnames(byhour)=="x"] <- "media"
	#print(byhour)
	#print(dati$PresenzeStimate)
	clock.plot(byhour$media,byhour$ora, main = paste("heatdial oraria, periodo: ",etichetta,sep=""),color="gray")
	dev.off()

	#3
	jpeg(paste(etichetta,"heatOraria.jpg",sep="-"),width=15,height=15,units="cm",res=resol)
	plot(byhour, main = paste("heatplot oraria, periodo: ",etichetta,sep=""),color="gray")
	axis(1,at=seq(minOra,23))
	dev.off()

	#4
	jpeg(paste(etichetta,"heatGsettimana-clockplot.jpg",sep="-"),width=15,height=15,units="cm",res=resol)
	byhour	<- aggregate(dati$NuoviIngressi,	by = list(dow=dati$Gsettimana), mean)
	byhour$dow <- wday(byhour$dow,label=TRUE)
	colnames(byhour)[colnames(byhour)=="x"] <- "media"
	clock.plot(byhour$media,byhour$dow, main = paste("heatdial giorni, periodo: ",etichetta,sep=""),color="gray")
	dev.off()

	#5
	jpeg(paste(etichetta,"heatGsettimana.jpg",sep="-"),width=15,height=15,units="cm",res=resol)
	plot(byhour, main = paste("heatplot giorni, periodo: ",etichetta,sep=""),color="gray")
	dev.off()

	#6
	jpeg(paste(etichetta,"heatSettimana-clockplot.jpg",sep="-"),width=15,height=15,units="cm",res=resol)
	byhour	<- aggregate(dati$NuoviIngressi,	by = list(settimana=dati$Settimana), mean)
	colnames(byhour)[colnames(byhour)=="x"] <- "media"
	clock.plot(byhour$media,byhour$settimana, main = paste("heatdial settimane, periodo: ",etichetta,sep=""),color="gray")
	dev.off()

	#7
	jpeg(paste(etichetta,"heatSettimana.jpg",sep="-"),width=15,height=15,units="cm",res=resol)
	plot(byhour, main = paste("heatplot settimane, periodo: ",etichetta,sep=""),color="gray")
	dev.off()

	#8
	if(FALSE){
	jpeg(paste(etichetta,"rapportoInOut.jpg",sep="-"),width=30,height=15,units="cm",res=resol)
	par(las=3) # xaxis label verticale
	d=dati[!is.infinite(dati$RapportoInOut),] # elimina orari inutili
	summ=summary(d)
	print(summ)
	plot(d$RapportoInOut~d$Data,type="h",xlab="",ylab="in/out")
	regressione <- lm(d$RapportoInOut ~ d$Data)
	abline(regressione, col = "blue", lwd = 4)
	legend("topright", paste("coefficiente trend line:", regressione$coefficients[2]), text.col = "blue")
	title(paste("Rapporto In/Out, errori di lettura (",etichetta,")",sep=""))
	axis.POSIXct(1,at = seq(min(d$Data), max(d$Data), "days"),format="%d/%m")
	dev.off()
	}
}

#plot(byhour,type="h",xlab="ora",ylab="media presenze",lwd=5)
#plot(byhour,type="h",xlab="ora",ylab="media presenze",lwd=5,xlim=c(8,24))
#rect(0, 0, 8, max(byhour$x), density = 70, col = "gray")
#title("Heat map orari")
###################################################################

#jpeg("heatMese.jpg",width=15,height=15,units="cm",res=resol)
##byhour <- aggregate(dati$PresenzeStimate, by = list(dati$Ora), mean)
#byhour <- aggregate(dati$Passaggi, by = list(dati$Mese), max)
#clock.plot(byhour$x, main = "Heat map mese",color="gray")
#dev.off()

###################################################################
for (anno in unique(year(dati$Data))){
  print(paste("=== ANNO: ",anno))
  for (mese in unique(month(dati$Data[year(dati$Data)==anno]))){
	  etichetta=paste(anno,mese,sep="-")
	  print(paste("--- mese: ",mese))
	  d=dati[year(dati$Data) == anno & month(dati$Data) == mese,]
	  genPlot(d,etichetta)
  }
  d=dati[year(dati$Data) == anno,]
  genPlot(d,anno)
}
