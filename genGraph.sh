#!/bin/bash

ZIP=GraficiEDatiFrequentazioneBibliotecaCormano.zip

logtmp=$(mktemp -p /run/user/$UID)

rm log.csv $ZIP *jpg

estraeStoria(){
 notmuch new 2>/dev/null

 #count=1
 #notmuch search --output=messages folder:".30_Generale.HackLabCormano.ContaPersoneLog"|
 notmuch search --output=messages subject:"log contapersone"|while
  read ID
 do
  notmuch show --part=1 $ID >> $logtmp
  #let count=count+1
 done 
}
estraeStoria

mergeAll(){
  echo Data,IngressiCumulo,UsciteCumulo > log.csv
  grep -rh ^T: $logtmp | awk 'length($0)>10' | tr -d "TIU:" | grep -v P | sort -n | uniq >> log.csv
  #grep -rh ^T: log.[[:digit:]]* | awk 'length($0)>10' | tr -d "TIU:" | grep -v P | sort -n | uniq >> log.csv
  #rm log.[[:digit:]]*
}
mergeAll

./plot.r
zip -q $ZIP *jpg *augm*csv
