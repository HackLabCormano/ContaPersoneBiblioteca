/**
 * legge impulsi sui due pin (fotocellule) per
 * capire se c'è stato un ingresso e in che direzione
 *
 * casi:
 * 1) h1,h2
 * 2) h2,h1
 * 3) h1,...
 * 4) h2,...
 * più le tempistiche!
 */

// per testing locale, usando pullup
#define TESTING
//#define DEBUG_INT
#define BLINK_DELAY 10
//#define COMPLETE_OUTPUT
#define LED D4 // LoLin

// pins (attenzione a sceglierli! non bisogna sovrapporsi ad esempio a LED_BUILTIN)
#define S1	D6 //D1
#define S2	D7 //D2

// tempo di muting fra una lettura e l'altra
const int mute=1000;

// timestamps
long h1=0; // high1
long h2=0; // high2
long lastFix=0; // timestamp ultima lettura valida
long last=0; // timestamp ultima lettura

// distanza tra impulsi
long diff=0;

void diffEazzera() {
    lastFix=millis();

    diff=h1-h2; // il segno dice se entrata/uscita

    h1=0;
    h2=0;
}

void azzeraTutto() {
    diff=0;
    h1=0;
    h2=0;
}

void intS1() {
    digitalWrite(LED, LOW); // accende

    if((millis()-lastFix)>mute) {
        h1=millis();
        last=h1;
#ifdef DEBUG_INT
        Serial.println(h1); // non è un problema stampare in interrupt
#endif

        if(h2!=0) // sono il secondo
            diffEazzera();
    }
}

void intS2() {
    digitalWrite(LED, LOW); // accende

    if((millis()-lastFix)>mute) {
        h2=millis();
        last=h2;
#ifdef DEBUG_INT
        Serial.println(h2); // non è un problema stampare in interrupt
#endif

        if(h1!=0) // sono il secondo
            diffEazzera();
    }
}

void attach() {
#ifdef TESTING
    attachInterrupt(digitalPinToInterrupt(S1),intS1, FALLING);
    attachInterrupt(digitalPinToInterrupt(S2),intS2, FALLING);
#else
    attachInterrupt(digitalPinToInterrupt(S1),intS1, RISING);
    attachInterrupt(digitalPinToInterrupt(S2),intS2, RISING);
#endif
}

void detach() {
    detachInterrupt(digitalPinToInterrupt(S1));
    detachInterrupt(digitalPinToInterrupt(S2));
}

//SETUP
void setup() {
    Serial.begin(115200);
    Serial.println("Booting...");

#ifdef TESTING
    pinMode(S1,INPUT_PULLUP);
    pinMode(S2,INPUT_PULLUP);
#else
    pinMode(S1,INPUT);
    pinMode(S2,INPUT);
#endif

    pinMode(LED, OUTPUT);

    attach();

    Serial.println("Booted!");
}

void printComplete() {
    Serial.print("S1:");
    Serial.print(h1);
    //Serial.print(",");
    //Serial.print(l1);

    Serial.print("\tS2:");
    Serial.print(h2);
    //Serial.print(",");
    //Serial.print(l2);

    /*
    Serial.print("\tdurata S1:");
    Serial.print(l1-h1);

    Serial.print("\tdurata S2:");
    Serial.print(l2-h2);
    */

    Serial.print("\tlastFix:");
    Serial.print(lastFix);
    Serial.print("\tlast:");
    Serial.print(last);

    Serial.print("\tlast diff:");
    Serial.print(diff);

    Serial.print("\tS1:");
    Serial.print(digitalRead(S1));
    Serial.print("\tS2:");
    Serial.print(digitalRead(S2));

    if(diff>0)
        Serial.println("\tuscita");
    else if(diff<0)
        Serial.println("\tentrata");
    else
        Serial.println("\tNULLA");
}

void printGraph() {
    /*
    Serial.print(h1);
    Serial.print(",");
    Serial.print(h2);
    Serial.print(",");

    Serial.print(lastFix);
    Serial.print(",");
    Serial.print(last);

    Serial.print(",");
    */

    Serial.println(diff);
}

////////////////////////////
//LOOP
void loop() {
    //if(millis()%1000)

#ifdef COMPLETE_OUTPUT
    printComplete();
#else
    printGraph();
#endif
    delay(mute/5);

    //digitalWrite(LED, LOW);
    //delay(BLINK_DELAY);
    digitalWrite(LED, HIGH);

    if((millis()-last) > mute*2)
        azzeraTutto();
}
